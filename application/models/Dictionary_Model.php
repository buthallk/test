<?php 

class Dictionary_Model extends CI_Model {

  public function __construct() {
    $this->load->database();
  }
    public function SetNewWord($word,$length) {
     $data = array(
            'word' => $word,
            'length'  => $length
    );
      return  $this->db->insert("dictionary", $data);
  }
    public function getDictionary($length) {
    $this->db->select('word');
    $query=$this->db->get_where('dictionary', array('length' => $length));
    return $query->result_array();
  }
   public function UploadDictionary($dictionaryFileName)
  {
    self::createDictionary();
    self::deleteDictionary();
    if (!is_readable($dictionaryFileName)) {
      return false;
    }
    $data = file_get_contents($dictionaryFileName);
    $data = explode("\r\n", $data);
    foreach ($data as $item): 
      self::SetNewWord(mb_convert_encoding($item, "UTF-8", "CP1251"),strlen($item));  
    endforeach;
    return true;
  }
   public function deleteDictionary() {
     $this->db->where('id>0');
     $this->db->delete('dictionary');
    }
    public function createDictionary() {
        $query = "CREATE TABLE IF NOT EXISTS dictionary(
                        id int(12) PRIMARY KEY AUTO_INCREMENT,
                        word  VARCHAR(24) NOT NULL ,
                        length int(12) NOT NULL,
                        UNIQUE(word)
                        )";
        $this->db->query($query);
    }
}