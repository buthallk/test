
<?php
    class WordMutation
    {
        private $dictionary;
        public function getDictionary()
        {
            return $this->dictionary;
        }
        public function setDictionary($dictionary)
        {
             $this->dictionary = $dictionary;
        }

         public function mutatationWord($from, $to)
        {
             if (!is_string($from) || !is_string($to)) {
                echo "Слова должны состоять только из букв";
            }
             if (mb_strlen($from) != mb_strlen($to)) {
                 echo "Слова должны быть одинаковой длины";
            }

            $graph = $this->createGraph();
            $resultSearch=$this->getMutatedWord($graph, $from, $to);
            return $resultSearch;
        }

        private function createGraph()
        {
            $graph = new CreateGraph($this->dictionary);
            return $graph->cteateGraph();
        }
        private function getMutatedWord(Graph $graph, $fromVertex, $toVertex )
        {
            $Nodes = $graph->getNodes();
            $passed = $this->getPassed($Nodes);

            $turn = $this->createTurn($fromVertex);
            $passed[$fromVertex] = true;

            $conversion = [];
            $conversion[$fromVertex] = $this->createConversionList($fromVertex);

            while ($this->notReached($toVertex, $turn)) {
                $vertex = $turn->dequeue();

                if ($this->vertexExists($Nodes, $vertex)) {
                    foreach ($Nodes[$vertex] as $relatedVertex) {
                        if (!$passed[$relatedVertex]) {
                            $turn->enqueue($relatedVertex);
                            $passed[$relatedVertex] = true;
                            $conversion[$relatedVertex] = clone $conversion[$vertex];
                            $conversion[$relatedVertex]->push($relatedVertex);
                        }
                    }
                }
            }

            return isset($conversion[$toVertex]) ? $this->toArray($conversion[$toVertex]) : [];
        }   
        private function createTurn($fromVertex)
        {
            $turn = new SplQueue();
            $turn->enqueue($fromVertex);
            return $turn;
        }
         private function createConversionList($vertex)
        {
            $list = new SplDoublyLinkedList();
            $list->setIteratorMode(SplDoublyLinkedList::IT_MODE_FIFO|SplDoublyLinkedList::IT_MODE_KEEP
            );
            $list->push($vertex);
            return $list;
        }
        private function notReached($toVertex, $turn)
        {
            return !$turn->isEmpty() && $turn->bottom() != $toVertex;
        }
        private function vertexExists($Nodes, $vertex)
        {
            return !empty($Nodes[$vertex]);
        }
         private function toArray(SplDoublyLinkedList $ways)
        {
            $transitions = [];
            foreach ($ways as $way) {
                $transitions[] = $way;
            }

            return $transitions;
        }
         private function getPassed($Nodes)
        {
            $passed = [];
            foreach ($Nodes as $relatedVertex => $relatedVertexes) {
                $passed[$relatedVertex] = false;
            }
            return $passed;
        }
    }
    class CreateGraph
    {
        const MATCHED_COUNT_LETTERS = 1;
        private $dictionary;
        public function __construct($dictionary)
        {
            $this->dictionary = $dictionary;
        }
        public function cteateGraph()
        {
            $graph = new Graph();

            foreach ($this->dictionary as $firstWord) {
                foreach($this->dictionary as $secondWord) {
                    if ($this->alike($firstWord, $secondWord)) {
                        $graph->addNode($firstWord, $secondWord);
                    }
                }
            }

            return $graph;
        }
        private function alike($firstWord, $secondWord)
        {
            $varianceLetters = count(
                array_diff_assoc(
                    $this->asArray($firstWord),
                    $this->asArray($secondWord)
                ));
            return $varianceLetters == self::MATCHED_COUNT_LETTERS;
        }
        private function asArray($string)
        {
             return preg_split('/(?<!^)(?!$)/u', $string);
        }



    }
    class Graph
    {
        private $nodes = [];
        public function addNode($fromVertex, $toVertex)
        {
            $this->nodes[$fromVertex][] = $toVertex;
        }
        public function getNodes()
        {
            return $this->nodes;
        }
    }