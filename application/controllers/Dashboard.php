<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends MY_Controller {	

	public function __construct() {
		parent::__construct();
		$this->load->model('dictionary_model');
		$this->load->library('wordmutation');


	}
	public function index()
	{
		$from='лужа';
		$to="море";
		$this->dictionary_model->UploadDictionary($_SERVER['DOCUMENT_ROOT'] . '/dictionary.txt');
		$dictionary=$this->dictionary_model->getDictionary(mb_strlen($from));
		foreach ($dictionary as $key => $value) {
			$data[ $key] =$value['word'];
		}
        $this->wordmutation->setDictionary($data);
        $conversion=$this->wordmutation->mutatationWord($from,$to);
		echo join('->', $conversion);
	}
}



